package com.game.rockpaperscissors.infrastructure;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.rockpaperscissors.application.playround.PlayRoundResponse;
import com.game.rockpaperscissors.application.playround.PlayRoundService;
import com.game.rockpaperscissors.domain.exception.HandException;
import com.game.rockpaperscissors.infrastructure.controller.PlayRoundController;

@RunWith(MockitoJUnitRunner.class)
public class PlayRoundControllerTest {
	
	private static final String HANDS_FOR_PLAYER_ONE = "handsForPlayerOne";
	private static final String HANDS_FOR_PLAYER_TWO = "handsForPlayerTwo";
	private static final String CONTROLLER_PATH = "/playround";
	private PlayRoundController playRoundController;
	private PlayRoundService playRoundService;
	private MockMvc mockMvc;
	
	@Before
	public void setUp() throws Exception {
		playRoundService = Mockito.mock(PlayRoundService.class);
		playRoundController = new PlayRoundController(playRoundService, new ObjectMapper());
		this.mockMvc = MockMvcBuilders.standaloneSetup(playRoundController).build();
	}
	
	@Test
	public void testPlayRoundIsOK() throws Exception {

		PlayRoundResponse playRoundResponse = new PlayRoundResponse("rock", "rock", "draw");		
		when(playRoundService.playRound(Mockito.any())).thenReturn(playRoundResponse);

		mockMvc.perform(post(CONTROLLER_PATH).param(HANDS_FOR_PLAYER_ONE, "0").param(HANDS_FOR_PLAYER_TWO,"0")).andExpect(status().isOk());

		verify(playRoundService, times(1)).playRound(Mockito.any());
	}
	
	@Test
	public void testPlayRoundIsInternalServerError() throws Exception {

		when(playRoundService.playRound(Mockito.any())).thenThrow(HandException.invalidTypeValue("invalidValue"));
		
		mockMvc.perform(post(CONTROLLER_PATH).param(HANDS_FOR_PLAYER_ONE, "0").param(HANDS_FOR_PLAYER_TWO,"0")).andExpect(status().isInternalServerError());

		verify(playRoundService, times(1)).playRound(Mockito.any());
	}
}
