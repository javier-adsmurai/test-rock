package com.game.rockpaperscissors.application.roundsummary;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.game.rockpaperscissors.domain.round.Round;
import com.game.rockpaperscissors.domain.round.RoundRepository;

public class RoundSummaryServiceTest {
	
	private RoundSummaryService roundSummayrService;
	private RoundRepository roundRepository;
	
	@BeforeClass
	public void setUp() throws Exception {
		roundRepository = Mockito.mock(RoundRepository.class);
		roundSummayrService = new RoundSummaryService(roundRepository);
	}
	
	@Test
	public void testItSummaryResponseIsOK() throws Exception {
		ArrayList<Round> roundList = generateRoundList();
		
		when(roundRepository.getAll()).thenReturn(roundList);
	
		RoundSummaryResponse roundSummaryResponse = roundSummayrService.roundSummary();
		
		Assert.assertEquals(roundSummaryResponse.getRoundsPlayed(), (Long) 6L);
		Assert.assertEquals(roundSummaryResponse.getWinsPlayerOne(), (Long) 3L);
		Assert.assertEquals(roundSummaryResponse.getWinsPlayerTwo(), (Long) 2L);
		Assert.assertEquals(roundSummaryResponse.getDraws(), (Long) 1L);
	}
	private ArrayList<Round> generateRoundList() {
		ArrayList<Round> roundList = new ArrayList<Round>();
		roundList.add(Round.playerOneWins());
		roundList.add(Round.playerOneWins());
		roundList.add(Round.playerOneWins());
		roundList.add(Round.playerTwoWins());
		roundList.add(Round.playerTwoWins());
		roundList.add(Round.playersDraws());
		return roundList;
	}
	
}
