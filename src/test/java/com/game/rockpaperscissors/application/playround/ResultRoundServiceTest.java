package com.game.rockpaperscissors.application.playround;

import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.game.rockpaperscissors.domain.exception.HandException;
import com.game.rockpaperscissors.domain.player.Hands;
import com.game.rockpaperscissors.domain.player.Player;
import com.game.rockpaperscissors.domain.player.ResultStatus;
import com.game.rockpaperscissors.domain.round.ResultRoundService;

public class ResultRoundServiceTest {

	private ResultRoundService resultRoundService;
	
	@BeforeClass
	public void setUp() throws Exception {
		resultRoundService = new ResultRoundService();
	}
	
	@Test(dataProvider = "playersHandsAndExpectedResult")
	public void testItResultStatusIsOK(String handPlayerOne, String handPlayerTwo, ResultStatus expectedResultStatus) throws HandException {
		Player playerOne = new Player(Arrays.asList(handPlayerOne));
		Player playerTwo = new Player(Arrays.asList(handPlayerTwo));
		
		ResultStatus resultStatus = resultRoundService.calculate(playerOne, playerTwo);
	
		Assert.assertEquals(resultStatus, expectedResultStatus);
	}
	
	@DataProvider(name = "playersHandsAndExpectedResult")
	public Object[][] dataProvider() {
		return new Object[][] {
			{String.valueOf(Hands.ROCK.getValue()), String.valueOf(Hands.ROCK.getValue()), ResultStatus.DRAW},
			{String.valueOf(Hands.ROCK.getValue()), String.valueOf(Hands.PAPER.getValue()), ResultStatus.PLAYER_TWO_WINS},
			{String.valueOf(Hands.ROCK.getValue()), String.valueOf(Hands.SCISSORS.getValue()), ResultStatus.PLAYER_ONE_WINS},
			{String.valueOf(Hands.PAPER.getValue()), String.valueOf(Hands.PAPER.getValue()), ResultStatus.DRAW},
			{String.valueOf(Hands.PAPER.getValue()), String.valueOf(Hands.ROCK.getValue()), ResultStatus.PLAYER_ONE_WINS},
			{String.valueOf(Hands.PAPER.getValue()), String.valueOf(Hands.SCISSORS.getValue()), ResultStatus.PLAYER_TWO_WINS},
			{String.valueOf(Hands.SCISSORS.getValue()),String.valueOf(Hands.SCISSORS.getValue()), ResultStatus.DRAW},
			{String.valueOf(Hands.SCISSORS.getValue()), String.valueOf(Hands.ROCK.getValue()), ResultStatus.PLAYER_TWO_WINS},
			{String.valueOf(Hands.SCISSORS.getValue()), String.valueOf(Hands.PAPER.getValue()), ResultStatus.PLAYER_ONE_WINS}
		};
	}
}