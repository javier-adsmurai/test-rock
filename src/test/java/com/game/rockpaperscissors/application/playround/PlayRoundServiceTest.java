package com.game.rockpaperscissors.application.playround;

import static org.mockito.Mockito.when;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.game.rockpaperscissors.application.playround.PlayRoundService;
import com.game.rockpaperscissors.domain.player.ResultStatus;
import com.game.rockpaperscissors.domain.round.ResultRoundService;
import com.game.rockpaperscissors.domain.round.RoundRepository;


public class PlayRoundServiceTest {

	private PlayRoundService playRoundService;
	private ResultRoundService resultRoundService;
	private RoundRepository roundRepository;
	
	@BeforeClass
	public void setUp() throws Exception {
		resultRoundService = Mockito.mock(ResultRoundService.class);
		roundRepository = Mockito.mock(RoundRepository.class);
		playRoundService = new PlayRoundService(resultRoundService, roundRepository);
	}
	
	@Test(dataProvider = "resulStatusAllowed")
	public void testItRoundResultIsOK(ResultStatus resultStatus) throws Exception {

		when(resultRoundService.calculate(Mockito.any(), Mockito.any())).thenReturn(resultStatus);
	
		PlayRoundRequest playRoundRequest = new PlayRoundRequest("0", "0");
		PlayRoundResponse playRoundResponse = playRoundService.playRound(playRoundRequest);
		
		Assert.assertEquals(playRoundResponse.getRoundResult(), resultStatus.getName());
	}
	
	@DataProvider(name = "resulStatusAllowed")
	public Object[][] dataProvider() {
		return new Object[][] {
			{ResultStatus.DRAW},
			{ResultStatus.PLAYER_TWO_WINS},
			{ResultStatus.PLAYER_ONE_WINS}
		};
	}
	
	@Test(expectedExceptions = Exception.class)
	public void testItThrowsExceptionByInvalidHandType() throws Exception {
		PlayRoundRequest playRoundRequest = new PlayRoundRequest("3", "0");
		playRoundService.playRound(playRoundRequest);

	}
}

