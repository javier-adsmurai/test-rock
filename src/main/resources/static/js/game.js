$( document ).ready(function() {

  $( "#btn-playround" ).click(function( event ) {   
    $( "#roundPlayed" ).removeClass('d-none');	
  	 
      event.preventDefault();

      $.ajax({
          data: {handsForPlayerOne: "0,1,2", handsForPlayerTwo: "0"},
          type: "POST",
          dataType: "json",
          url: "http://localhost:8080/playround",
      }).done(function( data, textStatus, jqXHR ) {
        var index = $("#playround tbody.addResult tr").length + 1;
        var newRowContent = "<tr><td>" + data.playerOneHand + "</td><td>" +data.playerTwoHand+ "</td><td>" +data.roundResult+ "</td></tr>";
  		  $("#playround tbody.addResult").append(newRowContent); 
  		  $("#roundPlayed span").html(index);
      }).fail(function( jqXHR, textStatus, errorThrown ) {
        alert("The request failed: " +  textStatus);
      });      

      $( "#playround" ).removeClass('d-none');
      $( "#info" ).addClass('d-none');
	
	});

  $( "#btn-resert" ).click(function( event ) {
  
    event.preventDefault();

    $("#roundPlayed span").html(0);	
    $("#playround tbody.addResult").html(''); 
    $( "#playround" ).removeClass('d-none');
    $( "#info" ).addClass('d-none');

  });	

  $( "#btn-info" ).click(function( event ) {
    $( "#roundPlayed" ).addClass('d-none');
  
  	event.preventDefault();

    $.getJSON("http://localhost:8080/roundsummary", function(data){
    	var newRowContent = "<tr><td>" + data.roundsPlayed + "</td><td>" +data.winsPlayerOne+ "</td><td>" +data.winsPlayerTwo+ "</td><td>" +data.draws+ "</td></tr>";
    	$( ".addInfo" ).html(newRowContent);
    });

    $( "#playround" ).addClass('d-none');
    $( "#info" ).removeClass('d-none');

  }); 

});