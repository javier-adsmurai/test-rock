package com.game.rockpaperscissors.domain.player;

import java.util.List;
import java.util.Random;

import com.game.rockpaperscissors.domain.exception.HandException;

public class Player {
	
	private int handPlayed;
	
	public Player(List<String> values) throws HandException {
		this.handPlayed = initByRandowValueRange(values);
	}

	private Integer initByRandowValueRange(List<String> values) throws HandException {
		String randowHand = values.get(new Random().nextInt(values.size()));
		
		try {
			return Integer.parseInt(randowHand);
			
		} catch (NumberFormatException ex) {
			throw HandException.invalidTypeValue(randowHand);
		}
	}
	
	public int getHandPlayed() {
		return this.handPlayed;
	}
}
