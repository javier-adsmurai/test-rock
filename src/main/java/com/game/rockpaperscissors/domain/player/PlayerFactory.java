package com.game.rockpaperscissors.domain.player;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PlayerFactory {
	public static List<String> getListHandsByString(String playerHands){
		return Arrays.stream(playerHands.split(",")).collect(Collectors.toList());
	}
}
