package com.game.rockpaperscissors.domain.player;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.game.rockpaperscissors.domain.exception.HandException;

public enum Hands {
	ROCK("Rock", 0), PAPER("Paper", 1), SCISSORS("Scissors", 2);
	
	private String hand;
	private int value;

	Hands(String hand, int value) {
		this.hand = hand;
		this.value = value;
	}

	public String getHand() {
		return hand;
	}
	
	public int getValue() {
		return value;
	}
	
	public static String getHandByValue(int value) throws HandException {
		
		final List<String> handList = getAllHandsByValue(value);
		
		if (handList.size() > 0) {
			return handList.get(0);
		}

		throw HandException.invalidValue(value);
	}

	private static List<String> getAllHandsByValue(int value) {
		return Arrays.stream(Hands.values()).
				filter(hand-> hand.getValue() == value).
				map(hand -> hand.getHand()).collect(Collectors.toList());
	}
}
