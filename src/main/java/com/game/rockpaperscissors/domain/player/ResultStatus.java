package com.game.rockpaperscissors.domain.player;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.game.rockpaperscissors.domain.exception.ResultStatusException;

public enum ResultStatus {
	PLAYER_ONE_WINS("Player 1 wins", 0), PLAYER_TWO_WINS("Player 2 wins", 1), DRAW("Draw", 2);
	
	private String name;
	private int value;
	
	ResultStatus(String name, int value) {
		this.name  = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}
	
	public int getValue() {
		return value;
	}
	
	public static String getStatusByValue(int value) throws ResultStatusException {
		
		final List<String> statusList = getAllStatusByValue(value);
		
		if (statusList.size() > 0) {
			return statusList.get(0);
		}
		
		throw ResultStatusException.invalidValue(value);
	}

	private static List<String> getAllStatusByValue(int value) {
		return Arrays.stream(ResultStatus.values()).
				filter(d-> d.getValue() == value).map(l -> l.getName()).collect(Collectors.toList());
	}
}
