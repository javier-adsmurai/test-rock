package com.game.rockpaperscissors.domain.exception;

public class HandException extends Exception {

	private static final String INVALID_VALUE_MESSAGE = "This hand value is invalid: ";
	private static final String INVALID_TYPE_VALUE_MESSAGE = "The hand value must be numerical. Value failure: ";
	private static final long serialVersionUID = 1395924175974842312L;

	public HandException(String message) {
		super(message);
	}
	
	public static HandException invalidValue(int value) {
		return new HandException(INVALID_VALUE_MESSAGE + value);
	}
	
	public static HandException invalidTypeValue(String value) {
		return new HandException(INVALID_TYPE_VALUE_MESSAGE + value);
	}
}
