package com.game.rockpaperscissors.domain.exception;

public class ResultStatusException extends Exception {

	private static final String INVALID_VALUE_MESSAGE = "This result status value is invalid: ";
	private static final long serialVersionUID = 1395924175974842312L;

	public ResultStatusException(String message) {
		super(message);
	}
	
	public static ResultStatusException invalidValue(int value) {
		return new ResultStatusException(INVALID_VALUE_MESSAGE + value);
	}
}
	