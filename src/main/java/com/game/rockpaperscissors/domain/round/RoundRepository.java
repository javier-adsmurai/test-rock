package com.game.rockpaperscissors.domain.round;

import java.util.ArrayList;

import com.game.rockpaperscissors.domain.round.Round;

public interface RoundRepository {
	
	public void save(Round round);
	
	public ArrayList<Round> getAll();
	
}
