package com.game.rockpaperscissors.domain.round;

import com.game.rockpaperscissors.domain.player.ResultStatus;

public class Round {
	
	private boolean playerOneWins;
	private boolean playerTwoWins;
	private boolean draw;
	
	private Round(boolean playerOneWins, boolean playerTwoWins, boolean draw) {
		this.playerOneWins = playerOneWins;
		this.playerTwoWins = playerTwoWins;
		this.draw = draw;
	}
	
	public static Round playerOneWins() {
		return new Round(true, false, false);
	}
	
	public static Round playerTwoWins() {
		return new Round(false, true, false);
	}
	
	public static Round playersDraws() {
		return new Round(false, false, true);
	}
	
	public static Round createRoundByResultStatus(ResultStatus resultStatus) {
		if (ResultStatus.PLAYER_ONE_WINS.equals(resultStatus)) {
			return playerOneWins();
		}
		
		if (ResultStatus.PLAYER_TWO_WINS.equals(resultStatus)) {
			return playerTwoWins();
		}
		
		return playersDraws();
	}

	public boolean isPlayerOneWins() {
		return playerOneWins;
	}

	public boolean isPlayerTwoWins() {
		return playerTwoWins;
	}

	public boolean isDraw() {
		return draw;
	}
}
