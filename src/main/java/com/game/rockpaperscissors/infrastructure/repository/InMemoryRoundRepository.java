package com.game.rockpaperscissors.infrastructure.repository;

import java.util.ArrayList;

import com.game.rockpaperscissors.domain.round.Round;
import com.game.rockpaperscissors.domain.round.RoundRepository;

public class InMemoryRoundRepository implements RoundRepository {
	
	ArrayList<Round> roundList = new ArrayList<>();
	
	public void save(Round round) {
		roundList.add(round);
	}
	
	public ArrayList<Round> getAll(){
		return roundList;
	}
}
