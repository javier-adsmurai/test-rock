package com.game.rockpaperscissors.infrastructure.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.game.rockpaperscissors.application.playround.PlayRoundRequest;
import com.game.rockpaperscissors.application.playround.PlayRoundResponse;
import com.game.rockpaperscissors.application.playround.PlayRoundService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class PlayRoundController {

	private final PlayRoundService playRoundService;
	
	private ObjectMapper objectMapper;

	public PlayRoundController(final PlayRoundService playRoundService, ObjectMapper objectMapper) {
		this.playRoundService = playRoundService;
		this.objectMapper = objectMapper;
	}
	
	@RequestMapping(
			value = "/playround", 
			method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> playRound(@RequestParam("handsForPlayerOne") String handsForPlayerOne, 
											@RequestParam("handsForPlayerTwo") String handsForPlayerTwo) {
		
		PlayRoundResponse playRoundResponse;
		String bodyResponse;
		
		try {
			PlayRoundRequest playRoundRequest = new PlayRoundRequest(handsForPlayerOne, handsForPlayerTwo);
			playRoundResponse = playRoundService.playRound(playRoundRequest);
			
			bodyResponse = objectMapper.writeValueAsString(playRoundResponse);
		} catch (Exception ex) {
			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<String>(bodyResponse, HttpStatus.OK);
	}
}
