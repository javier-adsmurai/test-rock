package com.game.rockpaperscissors.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.game.rockpaperscissors.domain.round.ResultRoundService;
import com.game.rockpaperscissors.infrastructure.repository.InMemoryRoundRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class RockPaperScissorsBeanConfig {

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
	
	@Bean
	public ResultRoundService resultRoundService() {
		return new ResultRoundService();
	}
	
	@Bean
	public InMemoryRoundRepository roundRespository() {
		return new InMemoryRoundRepository();
	}
	
}
