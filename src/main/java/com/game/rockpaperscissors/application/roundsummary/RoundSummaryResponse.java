package com.game.rockpaperscissors.application.roundsummary;

public class RoundSummaryResponse {
	private Long roundsPlayed;
	private Long winsPlayerOne;
	private Long winsPlayerTwo;
	private Long draws;
	
	public RoundSummaryResponse(Long roundsPlayed, Long winsPlayerOne, Long winsPlayerTwo, Long draws) {
		this.roundsPlayed = roundsPlayed;
		this.winsPlayerOne = winsPlayerOne;
		this.winsPlayerTwo = winsPlayerTwo;
		this.draws = draws;
	}

	public Long getRoundsPlayed() {
		return roundsPlayed;
	}

	public Long getWinsPlayerOne() {
		return winsPlayerOne;
	}

	public Long getWinsPlayerTwo() {
		return winsPlayerTwo;
	}

	public Long getDraws() {
		return draws;
	}
	
	
}
